#!/bin/bash
#Este script tem a função de trocar entre as versões do PHP (5.6 e 7.0 somente)
#Por enquanto é um trabalho em progresso (W.I.P.)

#Uso:
#Execute "sudo sh mysql_switch.sh [versão]"
#Versão precisa ser especificamente 5.6 ou 7.0

#O script checa qual é a versão do PHP instalada no sistema
#Se a versão instalada for a mesma digitada, está tudo OK
#Se a versão instalada for diferente então deverá haver a troca de versão

ver=$(php -v | cut -c 5-7 | head -n 1)
if [ "$ver" = "$1" ]; then
	echo "OK! Não é necessário fazer a troca de versão"
	echo "Versão atual do PHP:" $ver
else
	echo "Switching!"
	if [ "$1" = "5.6" ]; then
		sudo a2dismod php7.0; sudo a2enmod php5.6; sudo service apache2 restart
		sudo update-alternatives --set php /usr/bin/php5.6
	else
		sudo a2dismod php5.6; sudo a2enmod php7.0; sudo service apache2 restart
		sudo update-alternatives --set php /usr/bin/php7.0		
	fi
fi

#Incluir as seguintes linhas no script para fazer upgrade:
#sudo a2dismod php5.6 ; sudo a2enmod php7.0 ; sudo service apache2 restart
#sudo update-alternatives --set php /usr/bin/php7.0

#Incluir as seguintes linhas no script para fazer downgrade:
#sudo a2dismod php7.0 ; sudo a2enmod php5.6 ; sudo service apache2 restart
#sudo update-alternatives --set php /usr/bin/php5.6
